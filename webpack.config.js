module.exports = {
  entry: './src/javascript/main.js',
  output: {
    filename: './bundle.js'
  },
  target: 'web',
  node: {
    fs: "empty",
    module: "empty"
  }
};