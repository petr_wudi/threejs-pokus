import * as THREE from "three";

const TEXTURE_POLE = new THREE.TextureLoader().load( './img/pole.jpg' );
const TEXTURE_POLE_UPPER = new THREE.TextureLoader().load( './img/pole2.jpg' );

export function Box(x, y, z, boxType) {
    this.gridObject = { action: null };
    this.sceneObjects = [];
    this.generateSceneObjects(x, y, z, boxType);
}

Box.prototype.generateSceneObjects = function(x, y, z, boxType) {
    let height = 0;
    let texture = undefined;
    if (boxType === 1) {
        height = 5;
        texture = TEXTURE_POLE;
    } else if (boxType === 2) {
        height = 5;
        texture = TEXTURE_POLE_UPPER;
    } else if (boxType === 3) {
        height = 0.25;
        texture = TEXTURE_POLE_UPPER;
    }
    const material = new THREE.MeshLambertMaterial( { map: texture } );
    // make it box again
    this.createBoxPane(x, y, z, material, height, 0);
    this.createBoxPane(x, y, z, material, height, 1);
    this.createBoxPane(x, y, z, material, height, 2);
    this.createBoxPane(x, y, z, material, height, 3);
    if (boxType === 3) {
        this.createTopBoxPane(x, y, z, material, height);
        this.createBottomBoxPane(x, y, z, material);
    }
};

Box.prototype.createBoxPane = function(x, y, z, material, height, orientation) {
    const geometry = new THREE.PlaneGeometry(1, height);
    // geometry.computeFaceNormals();
    // geometry.computeVertexNormals();
    const cube = new THREE.Mesh(geometry, material);
    let switchX = 0;
    if (orientation === 1) {
        switchX = 1;
    }
    if (orientation === 3) {
        switchX = -1;
    }
    let switchY = 0;
    if (orientation === 0) {
        switchY = -1;
    }
    if (orientation === 2) {
        switchY = 1;
    }
    cube.position.x = x + 0.5 * switchX;
    cube.position.y = y + 0.5 * switchY;
    cube.position.z = z + height/2 - 0.5;
    cube.rotation.x = 1.57;
    cube.rotation.y = 1.57 * orientation;
    this.sceneObjects.push(cube);
};

Box.prototype.createTopBoxPane = function(x, y, z, material, position) {
    const geometry = new THREE.PlaneGeometry(1, 1);
    const cube = new THREE.Mesh(geometry, material);
    let switchX = 0;
    cube.position.x = x;
    cube.position.y = y;
    cube.position.z = z + position - 0.5;
    // cube.rotation.x = 1.57;
    // cube.rotation.y = 1.57 * orientation;
    this.sceneObjects.push(cube);
};

Box.prototype.createBottomBoxPane = function(x, y, z, material) {
    const geometry = new THREE.PlaneGeometry(1, 1);
    const cube = new THREE.Mesh(geometry, material);
    let switchX = 0;
    cube.position.x = x;
    cube.position.y = y;
    cube.position.z = z - 0.5;
    cube.rotation.x = 3.14;
    // cube.rotation.y = 1.57 * orientation;
    this.sceneObjects.push(cube);
};

Box.prototype.getSceneObjects = function() {
    return this.sceneObjects;
};

Box.prototype.getGridObject = function() {
    return this.gridObject;
};