import * as THREE from 'three';
const World = require('./world.js').World;

export function createWorld(jsonFile, scene) {
    const width = jsonFile.scene.width;
    const height = jsonFile.scene.height;
    const worldMap = new World(width, height, scene);

    for (let y = -8; y < 7; y += 4) {
        for (let x = -8; x < 7; x += 4) {
            worldMap.createCandle(x, y, scene);
        }
    }
    addFloors(worldMap, scene, jsonFile.floors);
    addBoundaries(scene, width, height);
    addCandles(worldMap, scene, jsonFile.candles);
    addStainedWindows(worldMap, scene, jsonFile.stainedWindows);
    addStairs(worldMap, scene, jsonFile.stairs);
    return worldMap;
}

const addBoundaries = function(scene, width, height) {
    /*for (let y = -10; y < 10; ++y) {
        window.worldMap.createBox(-10, y, scene);
        window.worldMap.createBox(9, y, scene);
    }
    for (let x = -9; x < 9; ++x) {
        window.worldMap.createBox(x, -10, scene);
        window.worldMap.createBox(x, 9, scene);
    }*/

    const textureFloor = new THREE.TextureLoader().load( './img/koberec.jpg' );
    const geometryFloor = new THREE.PlaneGeometry( width, height);
    // const geometryFloor = new THREE.BoxGeometry( 20, 20, 0);
    const materialFloor = new THREE.MeshLambertMaterial( { map: textureFloor } );
    const paneFloor = new THREE.Mesh(geometryFloor, materialFloor);
    // cubeFloor.position.z = -1;
    paneFloor.position.z = -0.5;
    scene.add(paneFloor);

    // window.worldMap.createBox(3, 0, scene);
    // window.worldMap.createBox(-3, 0, scene);
};

const addFloors = function(wm, scene, floors) {
    floors.forEach(function(floorJson) {
        const z = floorJson.z;
        const floor = wm.addFloor(z);
        if (floorJson.walls !== undefined) {
            addWalls(wm, floor, scene, floorJson.walls);
        }
        if (floorJson.poles !== undefined) {
            addPoles(wm, floor, scene, floorJson.poles);
        }
        if (floorJson.flooring !== undefined) {
            addFlooring(wm, floor, scene, floorJson.flooring);
        }
    });
};

const addWalls = function(wm, floor, scene, walls) {
    walls.forEach(function(wall) {
        const xStart = wall.xStart;
        const yStart = wall.yStart;
        const xEnd = wall.xEnd;
        const yEnd = wall.yEnd;
        const boxType = wall.type;
        for (let y = yStart; y <= yEnd; ++y) {
            for (let x = xStart; x <= xEnd; ++x) {
                wm.createBox(floor,x - wm.getShiftX(), y - wm.getShiftY(), boxType, scene);
            }
        }
    });
};

const addPoles = function(wm, floor, scene, poles) {
    poles.forEach(function(wall) {
        const x = wall.x;
        const y = wall.y;
        const type = wall.type;
        wm.createBox(floor,x - wm.getShiftX(), y - wm.getShiftY(), type, scene);
    });
};

const addFlooring = function(wm, floor, scene, floorings) {
    floorings.forEach(function(flooring) {
        const xStart = flooring.xStart;
        const yStart = flooring.yStart;
        const xEnd = flooring.xEnd;
        const yEnd = flooring.yEnd;
        if (xStart === undefined || yStart === undefined || xEnd === undefined || yEnd === undefined) {
            const x = flooring.x;
            const y = flooring.y;
            floor.createFlooring(x - wm.getShiftX(), y - wm.getShiftY());
        } else {
            for (let y = yStart; y <= yEnd; ++y) {
                for (let x = xStart; x <= xEnd; ++x) {
                    floor.createFlooring(x - wm.getShiftX(), y - wm.getShiftY());
                }
            }
        }
    });
};

const addCandles = function(wm, scene, candles) {
    candles.forEach(function(candle) {
        const x = candle.x;
        const y = candle.y;
        wm.createCandle(x - wm.getShiftX(), y - wm.getShiftY(), scene);
    });
};

const addStainedWindows = function(wm, scene, windows) {
    windows.forEach(function(stainedWindow) {
        const x = stainedWindow.x;
        const y = stainedWindow.y;
        const orientation = stainedWindow.orientation;
        wm.createStainedWindow(x - wm.getShiftX(), y - wm.getShiftY(), orientation, scene);
    });
};

const addStairs = function(wm, scene, stairsList) {
    stairsList.forEach(function(stairs) {
        const x = stairs.x;
        const y = stairs.y;
        wm.createStairs(x - wm.getShiftX(), y - wm.getShiftY());
    });
};
