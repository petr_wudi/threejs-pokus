const express = require('express');

const app = express();
app.use(express.static('src/public'));
app.use('/script', express.static('dist'));
app.use('/img', express.static('img'));

app.get('/', function (req, res) {
    res.sendFile('src/public/index.html');
});

port = 3000;
app.listen(port, function () {
    console.log(`Listening on port ${port}!`)
});
