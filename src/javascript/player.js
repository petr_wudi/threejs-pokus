import * as THREE from "three";

export function Player(scene, x, y) {
    this.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
    this.camera.rotation.x = 1.57;
    this.x = x;
    this.y = y;
    this.z = 0;
    this.rotation = 0;
    this.deleteme = null;
}

Player.prototype.getCamera = function() {
    return this.camera;
};

Player.prototype.move = function(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.deleteme.position.x = x;
    this.deleteme.position.y = y;
    this.deleteme.position.z = z;
    this.recomputeCamera();
};

Player.prototype.moveX = function(newX) {
    this.x = newX;
    this.deleteme.position.x = newX;
    this.recomputeCamera();
};

Player.prototype.moveY = function(newY) {
    this.y = newY;
    this.deleteme.position.y = newY;
    this.recomputeCamera();
};

Player.prototype.recomputeCamera = function() {
    const sinus = Math.sin(this.rotation);
    const cosine = Math.cos(this.rotation);
    const cameraShift = 1;
    const cameraX = this.x + cameraShift * sinus;
    const cameraY = this.y - cameraShift * cosine;
    this.camera.position.x = cameraX;
    this.camera.position.y = cameraY;
    this.camera.position.z = this.z;
};

Player.prototype.rotate = function(rotation) {
    this.rotation = rotation;
    this.camera.rotation.y = rotation;
    this.recomputeCamera();
};
