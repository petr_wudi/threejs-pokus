const Flooring = require('./flooring.js').Flooring;

export function Floor(gridWidth, gridHeight, z, scene) {
    this.z = z;
    this.grid = [];
    this.scene = scene;
    for (let y = 0; y < gridHeight; ++y) {
        this.grid[y] = new Array(gridWidth).fill(null);
    }
}

Floor.prototype.get = function(x, y) {
    return this.grid[y][x];
};

Floor.prototype.put = function(x, y, obj) {
    this.grid[y][x] = obj;
};

Floor.prototype.createFlooring = function(x, y) {
    return new Flooring(x, y, this.z - 0.5, this.scene);
};