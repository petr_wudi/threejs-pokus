import * as THREE from "three";
const HandLight = require('./handLight.js').HandLight;
import {Stairs} from './stairs.js';
const Floor = require('./floor.js').Floor;
import {Box} from "./box";

export let World = function (gridWidth, gridHeight, scene) {
    this.scene = scene;
    this.camera = null;
    this.cameraTop = null;
    this.floors = [];
    this.currentFloor = null;
    this.gridWidth = gridWidth;
    this.gridHeight = gridHeight;
    this.lights = [];
    this.bigLights = [];
    this.candles = [];
    this.minX = Math.floor(gridWidth / 2);
    this.minY = Math.floor(gridHeight / 2);
    this.movingFront = false;
    this.movingBack = false;
    this.turningLeft = false;
    this.turningRight = false;
    this.candleCount = 0;
    this.lightModeOn = false;
    this.handLight = new HandLight();

    this.player = null;
};

World.prototype.addFloor = function(z) {
    const floor = new Floor(this.gridWidth, this.gridHeight, z, this.scene);
    if (this.currentFloor == null) {
        this.currentFloor = floor;
    }
    this.floors.push(floor);
    return floor;
};

World.prototype.createBox = function(floor, x, y, boxType, scene) {
    const box = new Box(x, y, floor.z, boxType);
    box.getSceneObjects().forEach(function(sceneObj) {
        scene.add(sceneObj);
    });
    floor.put(x + this.minX, y + this.minY, box.getGridObject());
};

World.prototype.createStairs = function(x, y) {
    const bottomFloor = this.floors[0];
    const topFloor = this.floors[1];
    const stairs = new Stairs(x + 0.5, y + 0.5, bottomFloor, topFloor);
    stairs.addToScene(this.scene);
    this.addStairsToFloor(bottomFloor, stairs, x, y);
    this.addStairsToFloor(topFloor, stairs, x, y);

};

World.prototype.addStairsToFloor = function(floor, stairs, x, y) {
    this.currentFloor.put(x + this.minX, y + this.minY, stairs); // todo not current floor
    this.currentFloor.put(x + this.minX, y + this.minY + 1, stairs);
    this.currentFloor.put(x + this.minX + 1, y + this.minY, stairs);
    this.currentFloor.put(x + this.minX + 1, y + this.minY + 1, stairs);
};

World.prototype.getShiftX = function() {
    return this.minX;
};

World.prototype.getShiftY = function() {
    return this.minY;
};

World.prototype.addBox = function(box) {
    const x = box.position.x;
    const y = box.position.y;
    this.currentFloor.put(x + this.minX, y + this.minY, box);
};

World.prototype.createCandle = function(x, y, scene) {
    const cylinderHeight = 0.6;
    const cylinderZ = 3;
    const geometry = new THREE.CylinderGeometry( 0.05, 0.05, cylinderHeight, 8 );
    const material = new THREE.MeshLambertMaterial( {color: 0xcccccc} );
    const cylinder = new THREE.Mesh(geometry, material);
    cylinder.position.x = x;
    cylinder.position.y = y;
    cylinder.position.z = cylinderZ;
    cylinder.rotation.x = 1.57;
    const lightPositionZ = cylinderZ + cylinderHeight / 2 + 0.05;
    this.createCandleLight(x, y, lightPositionZ, scene);
    scene.add(cylinder);
};

World.prototype.createCandleLight = function(x, y, z, scene) {
    const intensity = 0.1;
    const light = new THREE.PointLight(0xffff33, intensity, 15);
    light.position.set(x, y, z);
    scene.add(light);
    this.addLight(light);
    this.candles.push(light);
    light.originalZ = z;
    light.originalIntensity = intensity;

    const miniLight = new THREE.PointLight(0xffaa33, 1, 0.2);
    light.position.set(x, y, z);
    scene.add(miniLight);
};

World.prototype.addLight = function(light) {
    this.lights.push(light);
};

World.prototype.createStainedWindow = function(x, y, orientation, scene) {
    const angle = (orientation * Math.PI) / 2;
    let lightMoveHorizontal = 0.2;
    let lightMoveVertical = 0;

    this.createStainedLight(x, y, orientation, 0, 0, 0xff0000);
    this.createStainedLight(x, y, orientation, 0, 0.4, 0x0000ff);
    this.createStainedLight(x, y, orientation, 0, -0.4, 0x0000ff);
    this.createStainedLight(x, y, orientation, 0.4, 0, 0x00ff00);
    this.createStainedLight(x, y, orientation, -0.4, 0, 0x00ff00);

    // const geometry = new THREE.SphereGeometry( 2, 8, 8 );
    // const material = new THREE.MeshLambertMaterial( {color: 0xcccccc} );
    // const sphere = new THREE.Mesh(geometry, material);
    // sphere.position.set(lightX, lightY, windowZ);
    // this.scene.add(sphere);

};

World.prototype.createStainedLight = function(x, y, orientation, lightMoveHorizontal, lightMoveVertical, color) {
    const intensity = 0.3;
    const intensity2 = 0.6;
    let lightX = x;
    let lightY = y;
    let lightZ = 5 + lightMoveVertical;
    if (orientation === 0) {
        lightY -= 0.55;
        lightX += lightMoveHorizontal;
    } else if (orientation === 1) {
        lightX -= 0.55;
        lightY += lightMoveHorizontal;
    } else if (orientation === 2) {
        lightY += 0.55;
        lightX += lightMoveHorizontal;
    } else if (orientation === 3) {
        lightX += 1;
        lightY += lightMoveHorizontal;
    }
    const light = new THREE.SpotLight(color, intensity, 20, 3);
    light.position.set(lightX, lightY, lightZ);
    const light2 = new THREE.PointLight(color, intensity2, 3, 2);
    this.scene.add(light);
    light2.position.set(lightX, lightY, lightZ);
    this.scene.add(light2);
    this.addLight(light);


    // const geometry = new THREE.SphereGeometry( 2, 8, 8 );
    // const material = new THREE.MeshLambertMaterial( {color: 0xcccccc} );
    // const sphere = new THREE.Mesh(geometry, material);
    // sphere.position.set(lightX, lightY, lightZ);
    // this.scene.add(sphere);
};

World.prototype.switchMode = function(isTop) {
    if (isTop) {
        window.worldMap.camera = window.worldMap.cameraTop;
    } else {
        window.worldMap.camera = this.player.getCamera();
    }
};

World.prototype.switchLightMode = function() {
    if (this.lightModeOn) {
        this.lightModeOn = false;
        const scene = this.scene;
        this.bigLights.forEach(function(light) {
            scene.remove(light);
        });
    } else {
        this.lightModeOn = true;
        if (this.bigLights.length === 0) {
            const spotLight = new THREE.SpotLight(0xffffff);
            spotLight.position.set(0, 0, 30);
            this.scene.add(spotLight);
            this.bigLights.push(spotLight);
            this.addLight(spotLight);

            const spotLight2 = new THREE.SpotLight(0xffffff);
            spotLight.position.set(2, -2, 30);
            this.scene.add(spotLight2);
            this.bigLights.push(spotLight2);
            this.addLight(spotLight2);
        } else {
            const scene = this.scene;
            this.bigLights.forEach(function(light) {
                scene.add(light);
            });
        }
    }
};

World.prototype.switchHandLightMode = function() {
    this.handLight.switchOnOff(this.scene);
};

World.prototype.moveRotate = function() {
    const speedFront = 0.1;
    const speedBack = 0.06;
    const turnSpeed = 0.07;
    if (this.movingFront && !this.movingBack) {
        this.move(speedFront);
    }
    if (!this.movingFront && this.movingBack) {
        this.move(-speedBack);
    }
    if (!this.turningLeft && this.turningRight) {
        this.rotate(turnSpeed);
    }
    if (this.turningLeft && !this.turningRight) {
        this.rotate(-turnSpeed);
    }
    this.moveCandles();
};

World.prototype.rotate = function(howMuch) {
    this.player.rotate(this.player.rotation - howMuch);
};

World.prototype.move = function(howMuch) {
    const rotation = this.player.rotation;
    const sinus = Math.sin(rotation);
    const cosine = Math.cos(rotation);
    const speedX = -sinus * howMuch;
    const speedY = cosine * howMuch;
    const newPositionX = this.player.x + speedX;
    const newPositionY = this.player.y + speedY;
    const collision = this.checkCollision(newPositionX, newPositionY);
    if (!collision.x) {
        this.player.moveX(newPositionX);
        this.handLight.moveToX(newPositionX);
    }
    if (!collision.y) {
        this.player.moveY(newPositionY);
        this.handLight.moveToY(newPositionY);
    }
};

World.prototype.checkCollision = function (x, y) {
    const personSize = 0.2;
    const xCenter = x + 0.5;
    const yCenter = y + 0.5;
    const xRef = Math.floor(xCenter);
    const yRef = Math.floor(y);
    const xMin = Math.floor(xCenter - personSize);
    const xMax = Math.floor(xCenter + personSize);
    const yMin = Math.floor(yCenter - personSize);
    const yMax = Math.floor(yCenter + personSize);
    let xCollision = false;
    let yCollision = false;
    let anyCollision = false;
    for (let yInt = yMin; yInt <= yMax; ++yInt) {
        for (let xInt = xMin; xInt <= xMax; ++xInt) {
            const box = this.getBox(xInt, yInt);
            if (box !== undefined && box !== null) {
                const action = box.action;
                if (action !== undefined && action !== null) {
                    const collides = box.action(this.player, x, y);
                    if (!collides) { // todo 2D
                        continue;
                    }
                }
                if (xInt === xRef) {
                    yCollision = true;
                } else if (yInt === yRef) {
                    xCollision = true;
                } else {
                    anyCollision = true;
                }
                // collision with the box
            }
        }
    }
    if (anyCollision && !xCollision && !yCollision) {
        yCollision = true;
        xCollision = true;
    }
    return {x: xCollision, y: yCollision};
};

World.prototype.moveCandles = function() {
    this.candleCount++;
    if (this.candleCount >= 2) {
        this.candleCount = 0;
        const random = Math.random();
        const randomInt = Math.floor(random * 3);
        this.candles.forEach(function(candle, index) {
            const maxChange = 0.02;
            const maxLightChange = 0.02;
            let changeZ = 0;
            let lightChange = 0;
            if ((index + randomInt) % 3 === 0) {
                changeZ = maxChange;
                lightChange = maxLightChange;
            } else if ((index + randomInt) % 3 === 1) {
                changeZ = -maxChange;
                lightChange = -maxLightChange;
            }
            candle.position.z = candle.originalZ + changeZ;
            candle.intensity = candle.originalIntensity + lightChange;
        });
        this.handLight.updateIntensity(random);
    }
};

World.prototype.getBox = function (x, y) {
    const xArr = x + this.minX;
    const yArr = y + this.minY;
    return this.currentFloor.get(xArr, yArr);
};