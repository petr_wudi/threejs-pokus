import * as THREE from 'three';
const Player = require('./player.js').Player;
const WorldFactory = require('./worldFactory.js');
const room1json = require('../rooms/01/room01.json');

const fps = 20;

const move = function(renderer, scene) {
    window.worldMap.moveRotate();
    redraw(renderer, scene, window.worldMap.camera);
};

const redraw = function(renderer, scene, camera) {
    renderer.render(scene, camera);
};

const init = function() {
    const scene = new THREE.Scene();
    const worldMap = WorldFactory.createWorld(room1json, scene);
    window.worldMap = worldMap;

    const renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );

    worldMap.player = new Player(scene, 20, 0);
    const cameraTop = new THREE.PerspectiveCamera( 50, window.innerWidth/window.innerHeight, 0.1, 1000 );
    cameraTop.position.z = 40;
    //cameraPlayer.position.y = 0;
    //cameraPlayer.position.x = 20; // todo delete
    //cameraPlayer.rotation.x = 1.57;

    worldMap.camera = worldMap.player.camera;
    worldMap.cameraTop = cameraTop;
    // window.worldMap.cameraPlayer = cameraPlayer;




    const geometry = new THREE.CylinderGeometry(0.1, 0.1, 1, 8, 8);
    const material = new THREE.MeshLambertMaterial( {color: 0xcc22cc} );
    const sphere = new THREE.Mesh(geometry, material);
    sphere.position.set(worldMap.player.x, worldMap.player.y, 0);
    scene.add(sphere);
    sphere.rotation.x = 1.57;
    worldMap.player.deleteme = sphere;

    window.setInterval(function(){ move(renderer, scene); }, 1000 / fps);
};

const switchMode = function(isTop) {
    window.worldMap.switchMode(isTop);
};

const onDocumentKeyDown = function(event) {
    const keyCode = event.which;
    if (keyCode === 38 || keyCode === 87) { // Up or W
        window.worldMap.movingFront = true;
    } else if (keyCode === 40 || keyCode === 83) { // Down or S
        window.worldMap.movingBack = true;
    } else if (keyCode === 37 || keyCode === 65) { // Left or A
        window.worldMap.turningLeft = true;
    } else if (keyCode === 39 || keyCode === 68) { // Right or D
        window.worldMap.turningRight = true;
    } else if (keyCode === 90) { // Key Z
        switchMode(true);
    }
};

const onDocumentKeyUp = function(event) {
    const keyCode = event.which;
    if (keyCode === 38 || keyCode === 87) { // Up or W
        window.worldMap.movingFront = false;
    } else if (keyCode === 40 || keyCode === 83) { // Down or S
        window.worldMap.movingBack = false;
    } else if (keyCode === 37 || keyCode === 65) { // Left or A
        window.worldMap.turningLeft = false;
    } else if (keyCode === 39 || keyCode === 68) { // Right or D
        window.worldMap.turningRight = false;
    } else if (keyCode === 75) { // Key K
        window.worldMap.switchLightMode();
    } else if (keyCode === 76) { // Key L
        window.worldMap.switchHandLightMode();
    } else if (keyCode === 90) { // Key Z
        switchMode(false);
    }
};

document.addEventListener("keydown", onDocumentKeyDown, false);
document.addEventListener("keyup", onDocumentKeyUp, false);
window.onload = init;
