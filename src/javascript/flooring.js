import * as THREE from "three";

const FLOOR_HEIGHT = 0.1;

export function Flooring(x, y, z, scene) {
    const geometry = new THREE.BoxGeometry(1, 1, FLOOR_HEIGHT);
    const textureStone = new THREE.TextureLoader().load( './img/stone.png' ); // todo move to dedicated class
    const material = new THREE.MeshLambertMaterial( {map: textureStone} );
    const cube = new THREE.Mesh(geometry, material);
    cube.position.set(x, y, z - FLOOR_HEIGHT / 2);
    scene.add(cube);
}