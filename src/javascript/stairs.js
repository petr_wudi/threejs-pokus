import * as THREE from "three";

const stepHeight = 0.1;
const zStart = -0.5;

export function Stairs(x, y, bottomFloor, topFloor) {
    this.x = x;
    this.y = y;
    this.radius = 2;
    const height = 5;
    const rotationDegrees = 180;
    const rotationRadians = 2 * Math.PI * rotationDegrees / 360;
    this.stairs = generateStairs(x, y, height, rotationRadians);
    this.stairsMap = generateStairsMap(this.stairs, bottomFloor, topFloor);
}

function generateStairs(x, y, height, initRotation) {
    const stairs = [];
    const numberOfStepsInCircle = 16;
    const rotationStep = (2 * Math.PI) / numberOfStepsInCircle;
    let previousStair = null;
    for (let i = 0; i * stepHeight < height; ++i) {
        const z = zStart + i * stepHeight;
        const rotationInt = i % numberOfStepsInCircle;
        const rotation = initRotation + rotationInt * rotationStep;
        const stair = new Stair(x, y, z, rotation);
        stair.previousStair = previousStair;
        if (previousStair !== null) {
            previousStair.nextStair = stair;
        }
        stairs.push(stair);
        previousStair = stair;
    }
    return stairs;
}

function generateStairsMap(stairs, bottomFloor, topFloor) {
    const stairsMap = new Map();
    let i = 1;
    stairs.forEach(function(stair) {
        // const z = stair.zTop;
        const z = i++;
        stairsMap.set(z, stair);
    });
    const firstStair = stairsMap.get(1);
    const noStairStart = {
        zTop: zStart,
        contains: function() { return true },
        nextStair: firstStair,
        previousStair: null,
        floor: bottomFloor
    };
    if (firstStair !== undefined) {
        firstStair.previousStair = noStairStart;
    }
    stairsMap.set(0, noStairStart);

    const lastStair = stairsMap.get(i - 1);
    const zEnd = zStart + stepHeight * (i - 1);
    const noStairEnd = new LastStair(zEnd, lastStair, topFloor);
    if (lastStair !== undefined) {
        lastStair.nextStair = noStairEnd;
    }
    stairsMap.set(i, noStairEnd);
    return stairsMap;
}

Stairs.prototype.addToScene = function(scene) {
    this.stairs.forEach(function(stair) {
        stair.addToScene(scene);
    });
    const lastStair = this.stairsMap.get(this.stairsMap.size - 1);
    lastStair.addToScene(scene);
};

Stairs.prototype.action = function(player, x, y) {
    const z = player.camera.position.z;
    const currentStair = this.getCurrentStep(z);
    const prevStair = currentStair.previousStair;
    const nextStair = currentStair.nextStair;

    if (nextStair !== null && nextStair.contains(x, y)) {
        // step up

        console.log('contains next stair', x, y);
        // console.log(nextStair.edgePoints[0], nextStair.edgePoints[1], nextStair.edgePoints[2], nextStair.edgePoints[3])
        const newZ = nextStair.zTop - zStart;
        player.move(x, y, newZ);
        if (nextStair.getRotation !== undefined) {
            player.rotate(nextStair.getRotation());
        }
        if (nextStair.floor !== undefined) {
            window.worldMap.currentFloor = nextStair.floor; // todo move currentFloor to player
        }
        return false;
    }
    if (currentStair.contains(x, y)) {
        console.log('current still contains', x, y);
        const newZ = currentStair.zTop - zStart;
        player.move(x, y, newZ);
        return false;
    }
    if (prevStair !== null && prevStair.contains(x, y)) {
        console.log('contains previous stair');
        const newZ = prevStair.zTop - zStart;
        player.move(x, y, newZ);
        if (prevStair.getRotation !== undefined) {
            player.rotate(prevStair.getRotation() + 3.14);
        }
        if (prevStair.floor !== undefined) {
            window.worldMap.currentFloor = prevStair.floor;
        }
        return false;
    }
    return true;
};

Stairs.prototype.getCurrentStep = function(z) {
    const zMore = z + Math.sign(z) * 0.0001; // just to eliminate epsilon
    const stepId = Math.floor(zMore / stepHeight);
    return this.stairsMap.get(stepId);

};

function Stair(xCenter, yCenter, zBottom, rotation) {
    const zCenter = zBottom + stepHeight / 2;
    this.zTop = zBottom + stepHeight;
    const stepDepth = 0.25;
    const stepWidth = 0.6;
    const geometry = new THREE.BoxGeometry(stepWidth, stepDepth, stepHeight);
    const textureStone = new THREE.TextureLoader().load( './img/stone.png' );
    const material = new THREE.MeshLambertMaterial( {map: textureStone} );
    this.cube = new THREE.Mesh(geometry, material);

    const cylinderRadius = 0.15;
    const cylinderRadiusCollision = cylinderRadius + 0.05;
    const cylinderGeometry = new THREE.CylinderGeometry(cylinderRadius, cylinderRadius, stepHeight, 8);
    const cylinderMaterial = new THREE.MeshLambertMaterial( {map: textureStone} );
    this.cylinder = new THREE.Mesh(cylinderGeometry, cylinderMaterial);
    this.cylinder.position.set(xCenter, yCenter, zCenter);
    this.cylinder.rotation.x = Math.PI * 0.5;

    const cosRotation = Math.cos(rotation);
    const sinRotation = Math.sin(rotation);
    const xMove = cosRotation * (stepWidth / 2);
    const yMove = sinRotation * (stepWidth / 2);
    const x = xCenter + xMove;
    const y = yCenter + yMove;
    this.cube.position.set(x, y, zCenter);
    this.cube.rotation.z = rotation;
    this.previousStair = null;
    this.nextStair = null;

    // generate edge points
    const xVec = { x: xMove, y: yMove };
    const xMoveShort = cosRotation * (stepWidth / 2  - cylinderRadiusCollision);
    const yMoveShort = sinRotation * (stepWidth / 2 - cylinderRadiusCollision);
    const xVecShort = { x: xMoveShort, y: yMoveShort }; // next to the column
    const yVec = { x: - yMove * stepDepth / stepWidth, y: xMove * stepDepth / stepWidth};
    const p1 = { x: x - xVecShort.x - yVec.x, y: y - xVecShort.y - yVec.y };
    const p2 = { x: x + xVec.x - yVec.x,      y: y + xVec.y - yVec.y };
    const p3 = { x: x + xVec.x + yVec.x,      y: y + xVec.y + yVec.y };
    const p4 = { x: x - xVecShort.x + yVec.x, y: y - xVecShort.y + yVec.y };
    this.edgePoints = [p1, p2, p3, p4];
}

Stair.prototype.addToScene = function(scene) {
    scene.add(this.cube);
    scene.add(this.cylinder);
    /*
    const zTop = this.zTop;
    let i = 0;
    this.edgePoints.forEach(function(pt) {
        const x = pt.x;
        const y = pt.y;
        const geometry = new THREE.SphereGeometry( 0.05, 8, 8);
        const material = new THREE.MeshLambertMaterial( {color: 0xff0000} );
        const asdf = new THREE.Mesh(geometry, material);
        asdf.position.set(x, y, zTop);
        scene.add(asdf);
        if (i === 0) {
            material.color.set(0xffff00);
        }
        if (i === 1) {
            material.color.set(0x0000ff);
        }
        i++;
    });
     */
};

Stair.prototype.contains = function(xa, ya) {
    const p1 = this.edgePoints[0];
    const p2 = this.edgePoints[1];
    const p3 = this.edgePoints[2];
    const p4 = this.edgePoints[3];
    const x1 = p1.x;
    const x2 = p2.x;
    const x3 = p3.x;
    const x4 = p4.x;
    const y1 = p1.y;
    const y2 = p2.y;
    const y3 = p3.y;
    const y4 = p4.y;

    // square:
    // p4 ------ p3
    // |         |
    // p1 ------ p2

    const l12 = normVector(xa, ya, x1, y1, x2, y2); // p1 - p2
    const l23 = normVector(xa, ya, x2, y2, x3, y3); // p2 - p3
    const l34 = normVector(xa, ya, x3, y3, x4, y4); // p3 - p4
    const l41 = normVector(xa, ya, x4, y4, x1, y1); // p4 - p1

    if (l12 <= 0) console.log('12', l12);
    if (l23 <= 0) console.log('23', l23);
    if (l34 <= 0) console.log('34', l34);
    if (l41 <= 0) console.log('41', l41);
    if (!(l12 > 0 && l23 > 0 && l34 > 0 && l41 > 0)) {
        console.log(xa, ya);
        console.log(p1, p2, p3, p4);
    }
    return l12 > 0 && l23 > 0 && l34 > 0 && l41 > 0;
};

Stair.prototype.getRotation = function() {
    return this.cube.rotation.z;
};

function normVector(xa, ya, x1, y1, x2, y2) {
    const x21 = (x2 - x1);
    const y21 = (y2 - y1);
    return (-xa * y21 + ya * x21 + x1*y2 - x2*y1) / (x21 * x21 + y21 * y21);
}

function LastStair(zEnd, lastStair, topFloor) {
    this.zTop = zEnd;
    this.previousStair = lastStair;
    this.nextStair = null;
    this.floor = topFloor;
    const stepDepth = 0.25;
    const stepWidth = 0.6;
    const myDepth = 1.42;
    const additionalWidth = 1.42;
    const geometry = new THREE.BoxGeometry(stepWidth + additionalWidth, myDepth, stepHeight);
    const textureStone = new THREE.TextureLoader().load( './img/stone.png' );
    const material = new THREE.MeshLambertMaterial( {map: textureStone} );
    this.cube = new THREE.Mesh(geometry, material);


    const rotation = lastStair.cube.rotation.z;
    const xCenter = lastStair.cylinder.position.x;
    const yCenter = lastStair.cylinder.position.y;
    const zCenter = lastStair.cylinder.position.z;
    const cosRotation = Math.cos(rotation);
    const sinRotation = Math.sin(rotation);
    const xMove = cosRotation * (stepWidth - additionalWidth / 2) - sinRotation * ((myDepth + stepDepth) / 2);
    const yMove = sinRotation * (stepWidth - additionalWidth / 2) + cosRotation * ((myDepth + stepDepth) / 2);
    const x = xCenter + xMove;
    const y = yCenter + yMove;
    this.cube.position.set(x, y, zCenter);
    this.cube.rotation.z = rotation;
}

LastStair.prototype.contains = function() {
    if (this.previousStair === undefined) {
        return true;
    }
    return !this.previousStair.contains();
};

LastStair.prototype.addToScene = function(scene) {
    scene.add(this.cube);
};
