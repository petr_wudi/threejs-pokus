import * as THREE from "three";

export let HandLight = function () {
    this.medianIntensity = 0.4;
    this.intensityStd = 0.025;
    this.handLight = new THREE.PointLight(0xffaa33, this.medianIntensity);
    this.handLight.decay = 2;
    this.handLight.distance = 10;
    this.isTurnedOn = false;
};

HandLight.prototype.switchOnOff = function (scene) {
    if (this.isTurnedOn) {
        this.isTurnedOn = false;
        scene.remove(this.handLight);
    } else {
        this.isTurnedOn = true;
        scene.add(this.handLight);
    }
};

HandLight.prototype.moveToX = function (newPositionX) {
    this.handLight.position.x = newPositionX;
};

HandLight.prototype.moveToY = function (newPositionY) {
    this.handLight.position.y = newPositionY;
};

HandLight.prototype.updateIntensity = function(random) {
    const rand2 = Math.floor(random * 10) / 10;
    this.handLight.intensity = this.medianIntensity - this.intensityStd + (rand2 * this.intensityStd * 2);
};
